package com.devcamp.arrayflterinputrestapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service
public class RainbowService {
    private int[] largerNumbers = { 1, 23, 32, 43, 54, 65, 86, 10, 15, 16, 18 };

    public ArrayList<Integer> getArrIntByPos(int pos) {
        ArrayList<Integer> result = new ArrayList<>();
        for (Integer intElement : largerNumbers) {
            if (intElement > pos) {
                result.add(intElement);
            }
        }
        return result;
    }

    public Integer getArrIntByIndex(@PathVariable("id") int index) {
        if (index >= 0 && index < 10) {
            return largerNumbers[index];
        } else {
            return null;
        }
    }
}
