package com.devcamp.arrayflterinputrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArrayflterinputrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArrayflterinputrestapiApplication.class, args);
	}

}
