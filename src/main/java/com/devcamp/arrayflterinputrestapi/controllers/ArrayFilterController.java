package com.devcamp.arrayflterinputrestapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.arrayflterinputrestapi.services.RainbowService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ArrayFilterController {
    @Autowired
    RainbowService rainbowService;

    @GetMapping("/array-int-request-query")
    public ArrayList<Integer> getArrIntByPos(@RequestParam(required = true) int pos) {
        return rainbowService.getArrIntByPos(pos);
    }

    @GetMapping("/array-int-param/{index}")
    public Integer getArrIntByIndex (@PathVariable() int index){
        return rainbowService.getArrIntByIndex(index);
    }
}
